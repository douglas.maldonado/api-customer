package br.com.customer.customer.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cliente não Encontrado")
public class CustomerNotFoundException extends RuntimeException{
}
