package br.com.customer.customer.controllers;

import br.com.customer.customer.models.Customer;
import br.com.customer.customer.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Customer cadastrarUsuario(@RequestBody @Valid Customer customer){
        Customer customerObjeto = customerService.salvarUsuario(customer);

        return customerObjeto;
    }

    @GetMapping("/{id}")
    public Customer consultarPorId (@PathVariable(name = "id") Long id){
//        try{
            Customer customer = customerService.buscarUsuarioPorId(id);
            return customer;
//        }catch (RuntimeException exception){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
//        }
    }




}
