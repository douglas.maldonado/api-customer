package br.com.customer.customer.services;

import br.com.customer.customer.exceptions.CustomerNotFoundException;
import br.com.customer.customer.models.Customer;
import br.com.customer.customer.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Customer salvarUsuario(Customer customer){

        Customer customerObjeto = customerRepository.save(customer);

        return customerObjeto;
    }

    public Customer buscarUsuarioPorId(Long id){

        Optional<Customer> optionalCustomer = customerRepository.findById(id);

        if(optionalCustomer.isPresent()){
            return  optionalCustomer.get();
        }

        throw new CustomerNotFoundException();

    }
}
